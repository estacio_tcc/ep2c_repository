package br.com.ep2c.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "produto")
@NamedQueries({
	@NamedQuery(name = ProdutoEntity.findNamesByLikeName, query = "select e.nome from ProdutoEntity e where e.nome like :nome "),
	@NamedQuery(name = ProdutoEntity.findByName, query = "select e from ProdutoEntity e where e.nome = :nome ") })
public class ProdutoEntity implements IEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 3804890510566534441L;

    public static final String findNamesByLikeName = "findNamesByLikeName";
    public static final String findByName = "findByName";

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "nome", nullable = false, length = 50, unique = true)
    private String nome;

    @Column(name = "descricao", length = 100)
    private String descricao;

    @Column(name = "categoria", length = 50)
    private String categoria;

    @Column(name = "fornecedor", length = 50)
    private String fornecedor;

    @Column(name = "marca", length = 50)
    private String marca;

    @Column(name = "volume", length = 50)
    private String volume;

    @Column(name = "codigoFiscal", length = 50)
    private String codigoFiscal;

    @Column(name = "preco", length = 50)
    private BigDecimal preco;

    public String getNome() {
	return nome;
    }

    public void setNome(String nome) {
	this.nome = nome;
    }

    public String getDescricao() {
	return descricao;
    }

    public void setDescricao(String descricao) {
	this.descricao = descricao;
    }

    public String getCategoria() {
	return categoria;
    }

    public void setCategoria(String categoria) {
	this.categoria = categoria;
    }

    public String getFornecedor() {
	return fornecedor;
    }

    public void setFornecedor(String fornecedor) {
	this.fornecedor = fornecedor;
    }

    public String getMarca() {
	return marca;
    }

    public void setMarca(String marca) {
	this.marca = marca;
    }

    public String getVolume() {
	return volume;
    }

    public void setVolume(String volume) {
	this.volume = volume;
    }

    public String getCodigoFiscal() {
	return codigoFiscal;
    }

    public void setCodigoFiscal(String codigoFiscal) {
	this.codigoFiscal = codigoFiscal;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Integer getId() {
	return id;
    }

    public void setPreco(BigDecimal preco) {
	this.preco = preco;
    }

    public BigDecimal getPreco() {
	return preco;
    }

    @Override
    public String toString() {
	return "ProdutoEntity [id=" + id + ", nome=" + nome + ", descricao=" + descricao
		+ ", categoria=" + categoria + ", fornecedor=" + fornecedor + ", marca="
		+ marca + ", volume=" + volume + ", codigoFiscal=" + codigoFiscal
		+ ", preco=" + preco + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nome == null) ? 0 : nome.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ProdutoEntity other = (ProdutoEntity) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nome == null) {
	    if (other.nome != null)
		return false;
	} else if (!nome.equals(other.nome))
	    return false;
	return true;
    }

}
