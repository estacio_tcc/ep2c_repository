package br.com.ep2c.model;

import java.io.Serializable;

public interface IEntity extends Serializable {

    public Integer getId();
}
