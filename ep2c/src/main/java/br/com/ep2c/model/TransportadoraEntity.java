package br.com.ep2c.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "transportadora")
public class TransportadoraEntity implements IEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 7106508329745185549L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "nome", nullable = false, length = 50)
    private String nomeTransportadora;

    @ManyToOne
    @JoinColumn(name = "endereco", referencedColumnName = "id")
    @Cascade({ org.hibernate.annotations.CascadeType.SAVE_UPDATE,
	    org.hibernate.annotations.CascadeType.DELETE })
    private EnderecoEntity endereco;

    @Column(name = "cnpj", nullable = false, length = 14)
    private String cnpj;

    @Column(name = "regiao_atendida", length = 200)
    private String regiaoAtendida;

    @OneToMany(mappedBy = "transportadora", targetEntity = VeiculoEntity.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<VeiculoEntity> veiculos;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getNomeTransportadora() {
	return nomeTransportadora;
    }

    public void setNomeTransportadora(String nomeTransportadora) {
	this.nomeTransportadora = nomeTransportadora;
    }

    public EnderecoEntity getEndereco() {
	return endereco;
    }

    public void setEndereco(EnderecoEntity endereco) {
	this.endereco = endereco;
    }

    public String getCnpj() {
	return cnpj;
    }

    public void setCnpj(String cnpj) {
	this.cnpj = cnpj;
    }

    public String getRegiaoAtendida() {
	return regiaoAtendida;
    }

    public void setRegiaoAtendida(String regiaoAtendida) {
	this.regiaoAtendida = regiaoAtendida;
    }

    public List<VeiculoEntity> getVeiculos() {
	return veiculos;
    }

    public void setVeiculos(List<VeiculoEntity> veiculos) {
	this.veiculos = veiculos;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	TransportadoraEntity other = (TransportadoraEntity) obj;
	if (cnpj == null) {
	    if (other.cnpj != null)
		return false;
	} else if (!cnpj.equals(other.cnpj))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}
