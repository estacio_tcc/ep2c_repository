package br.com.ep2c.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "login")
public class LoginEntity implements IEntity {

    /**
    * 
    */
    private static final long serialVersionUID = 7633509394364216019L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getLogin() {
	return login;
    }

    public void setLogin(String login) {
	this.login = login;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }
    // public UsuarioEntity getUsuario() {
    // return usuario;
    // }
    // public void setUsuario(UsuarioEntity usuario) {
    // this.usuario = usuario;
    // }

    public LoginEntity clone() {
	LoginEntity l = new LoginEntity();
	l.setId(id);
	l.setLogin(login);
	l.setPassword(password);
	return l;

    }

}
