package br.com.ep2c.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "pedido")
public class PedidoEntity implements IEntity {

    /**
     * 
     */
    private static final long serialVersionUID = -7419451687848101269L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "data_pedido", nullable = false)
    private Date dataPedido;

    @ManyToOne
    @JoinColumn(name = "cliente", referencedColumnName = "id")
    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    private ClienteEntity cliente;

    @Column(name = "codigo_pagamento", nullable = false, length = 100)
    private String codigoPagamento;

    @OneToMany(mappedBy = "pedido")
    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    private List<ItemPedidoEntity> itens;

    @ManyToOne
    @JoinColumn(name = "funcionario", referencedColumnName = "id")
    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    private FuncionarioEntity funcionario;

    @Column(name = "valor_total")
    private BigDecimal valorTotal;

    @Column(name = "peso_total")
    private Double pesoTotal;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Date getDataPedido() {
	return dataPedido;
    }

    public void setDataPedido(Date dataPedido) {
	this.dataPedido = dataPedido;
    }

    public ClienteEntity getCliente() {
	return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
	this.cliente = cliente;
    }

    public String getCodigoPagamento() {
	return codigoPagamento;
    }

    public void setCodigoPagamento(String codigoPagamento) {
	this.codigoPagamento = codigoPagamento;
    }

    public List<ItemPedidoEntity> getItens() {
	return itens;
    }

    public void setItens(List<ItemPedidoEntity> itens) {
	this.itens = itens;
    }

    public FuncionarioEntity getFuncionario() {
	return funcionario;
    }

    public void setFuncionario(FuncionarioEntity funcionario) {
	this.funcionario = funcionario;
    }

    public BigDecimal getValorTotal() {
	return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
	this.valorTotal = valorTotal;
    }

    public Double getPesoTotal() {
	return pesoTotal;
    }

    public void setPesoTotal(Double pesoTotal) {
	this.pesoTotal = pesoTotal;
    }

    @Override
    public String toString() {
	return "PedidoEntity [id=" + id + ", dataPedido=" + dataPedido + ", cliente="
		+ cliente + ", codigoPagamento=" + codigoPagamento + ", itens="
		+ (itens != null ? itens.size() : "is null") + ", funcionario="
		+ funcionario + ", valorTotal=" + valorTotal + ", pesoTotal=" + pesoTotal
		+ "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	PedidoEntity other = (PedidoEntity) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}
