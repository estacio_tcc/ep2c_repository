package br.com.ep2c.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "item_pedido")
public class ItemPedidoEntity implements IEntity {

    /**
     * 
     */
    private static final long serialVersionUID = -7419451687848101269L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "pedido", referencedColumnName = "id")
    private PedidoEntity pedido;

    @ManyToOne
    @JoinColumn(name = "produto", referencedColumnName = "id")
    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    private ProdutoEntity produto;

    @Column(name = "quantidade")
    private Double quantidade;

    @Column(name = "preco_unitario")
    private BigDecimal precoUnitario;

    public Integer getId() {
	return id;
    }

    public PedidoEntity getPedido() {
	return pedido;
    }

    public void setPedido(PedidoEntity pedido) {
	this.pedido = pedido;
    }

    public ProdutoEntity getProduto() {
	return produto;
    }

    public void setProduto(ProdutoEntity produto) {
	this.produto = produto;
    }

    public Double getQuantidade() {
	return quantidade;
    }

    public void setQuantidade(Double quantidade) {
	this.quantidade = quantidade;
    }

    public BigDecimal getPrecoUnitario() {
	return precoUnitario;
    }

    public void setPrecoUnitario(BigDecimal precoUnitario) {
	this.precoUnitario = precoUnitario;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Override
    public String toString() {
	return "ItemPedidoEntity [id=" + id + ", pedido=" + pedido + ", produto="
		+ produto + ", quantidade=" + quantidade + ", precoUnitario="
		+ precoUnitario + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((pedido == null) ? 0 : pedido.hashCode());
	result = prime * result
		+ ((precoUnitario == null) ? 0 : precoUnitario.hashCode());
	result = prime * result + ((produto == null) ? 0 : produto.hashCode());
	result = prime * result + ((quantidade == null) ? 0 : quantidade.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ItemPedidoEntity other = (ItemPedidoEntity) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (pedido == null) {
	    if (other.pedido != null)
		return false;
	} else if (!pedido.equals(other.pedido))
	    return false;
	if (precoUnitario == null) {
	    if (other.precoUnitario != null)
		return false;
	} else if (!precoUnitario.equals(other.precoUnitario))
	    return false;
	if (produto == null) {
	    if (other.produto != null)
		return false;
	} else if (!produto.equals(other.produto))
	    return false;
	if (quantidade == null) {
	    if (other.quantidade != null)
		return false;
	} else if (!quantidade.equals(other.quantidade))
	    return false;
	return true;
    }

}
