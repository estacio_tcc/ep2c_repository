package br.com.ep2c.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "endereco")
public class EnderecoEntity implements IEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 7616194846847571958L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "tipoLogradouro", nullable = false)
    private String tipoLogradouro;

    @Column(name = "logradouro", nullable = false)
    private String logradouro;

    @Column(name = "numero")
    private String numero;

    @Column(name = "bairro", nullable = false)
    private String bairro;

    @Column(name = "cidade", nullable = false)
    private String cidade;

    @Column(name = "uf", nullable = false)
    private String uf;

    @Column(name = "complemento")
    private String complemento;

    @Column(name = "cep")
    private String cep;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getTipoLogradouro() {
	return tipoLogradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
	this.tipoLogradouro = tipoLogradouro;
    }

    public String getLogradouro() {
	return logradouro;
    }

    public void setLogradouro(String logradouro) {
	this.logradouro = logradouro;
    }

    public String getNumero() {
	return numero;
    }

    public void setNumero(String numero) {
	this.numero = numero;
    }

    public String getBairro() {
	return bairro;
    }

    public void setBairro(String bairro) {
	this.bairro = bairro;
    }

    public String getCidade() {
	return cidade;
    }

    public void setCidade(String cidade) {
	this.cidade = cidade;
    }

    public String getUf() {
	return uf;
    }

    public void setUf(String uf) {
	this.uf = uf;
    }

    public String getComplemento() {
	return complemento;
    }

    public void setComplemento(String complemento) {
	this.complemento = complemento;
    }

    public String getCep() {
	return cep;
    }

    public void setCep(String cep) {
	this.cep = cep;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((bairro == null) ? 0 : bairro.hashCode());
	result = prime * result + ((cep == null) ? 0 : cep.hashCode());
	result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
	result = prime * result + ((complemento == null) ? 0 : complemento.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((logradouro == null) ? 0 : logradouro.hashCode());
	result = prime * result + ((numero == null) ? 0 : numero.hashCode());
	result = prime * result
		+ ((tipoLogradouro == null) ? 0 : tipoLogradouro.hashCode());
	result = prime * result + ((uf == null) ? 0 : uf.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	EnderecoEntity other = (EnderecoEntity) obj;
	if (bairro == null) {
	    if (other.bairro != null)
		return false;
	} else if (!bairro.equals(other.bairro))
	    return false;
	if (cep == null) {
	    if (other.cep != null)
		return false;
	} else if (!cep.equals(other.cep))
	    return false;
	if (cidade == null) {
	    if (other.cidade != null)
		return false;
	} else if (!cidade.equals(other.cidade))
	    return false;
	if (complemento == null) {
	    if (other.complemento != null)
		return false;
	} else if (!complemento.equals(other.complemento))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (logradouro == null) {
	    if (other.logradouro != null)
		return false;
	} else if (!logradouro.equals(other.logradouro))
	    return false;
	if (numero == null) {
	    if (other.numero != null)
		return false;
	} else if (!numero.equals(other.numero))
	    return false;
	if (tipoLogradouro == null) {
	    if (other.tipoLogradouro != null)
		return false;
	} else if (!tipoLogradouro.equals(other.tipoLogradouro))
	    return false;
	if (uf == null) {
	    if (other.uf != null)
		return false;
	} else if (!uf.equals(other.uf))
	    return false;
	return true;
    }

}
