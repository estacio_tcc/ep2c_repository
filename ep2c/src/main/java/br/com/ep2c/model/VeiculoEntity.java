package br.com.ep2c.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "veiculo")
public class VeiculoEntity implements IEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 1298880370167832016L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "montadora", length = 45)
    private String montadora;

    @Column(name = "modelo", length = 45)
    private String modelo;

    @Column(name = "placa", length = 7)
    private String placa;

    @Column(name = "ano", length = 4)
    private Integer ano;

    @ManyToOne
    @JoinColumn(name = "transportadora", referencedColumnName = "id")
    @Cascade({ CascadeType.ALL })
    private TransportadoraEntity transportadora;

    public String getMontadora() {
	return montadora;
    }

    public void setMontadora(String montadora) {
	this.montadora = montadora;
    }

    public String getModelo() {
	return modelo;
    }

    public void setModelo(String modelo) {
	this.modelo = modelo;
    }

    public String getPlaca() {
	return placa;
    }

    public void setPlaca(String placa) {
	this.placa = placa;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Integer getId() {
	return id;
    }

    public TransportadoraEntity getTransportadora() {
	return transportadora;
    }

    public void setTransportadora(TransportadoraEntity transportadora) {
	this.transportadora = transportadora;
    }

    public Integer getAno() {
	return ano;
    }

    public void setAno(Integer ano) {
	this.ano = ano;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((placa == null) ? 0 : placa.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	VeiculoEntity other = (VeiculoEntity) obj;
	if (placa == null) {
	    if (other.placa != null)
		return false;
	} else if (!placa.equals(other.placa))
	    return false;
	return true;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
	VeiculoEntity clone = new VeiculoEntity();
	clone.setId(this.id);
	clone.setAno(this.ano);
	clone.setModelo(this.modelo);
	clone.setMontadora(this.montadora);
	clone.setPlaca(this.placa);
	clone.setTransportadora(this.transportadora);
	return clone;
    }
}
