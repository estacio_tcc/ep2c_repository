package br.com.ep2c.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "funcionario")
@PrimaryKeyJoinColumn(name = "id")
@NamedQueries({
	@NamedQuery(name = FuncionarioEntity.findByNotUsed, query = "SELECT f FROM FuncionarioEntity f "
		+ "where f.id not in (SELECT u.funcionario.id FROM UsuarioEntity u)") })
public class FuncionarioEntity extends PessoaEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 5802166405984786839L;

    public static final String findByNotUsed = "findByNotUsed";

    @Column(name = "data_nascimento")
    private Date dataNascimento;

    public Date getDataNascimento() {
	return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
	this.dataNascimento = dataNascimento;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = super.hashCode();
	result = prime * result
		+ ((dataNascimento == null) ? 0 : dataNascimento.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (!super.equals(obj))
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	FuncionarioEntity other = (FuncionarioEntity) obj;
	if (dataNascimento == null) {
	    if (other.dataNascimento != null)
		return false;
	} else if (!dataNascimento.equals(other.dataNascimento))
	    return false;
	return true;
    }

}
