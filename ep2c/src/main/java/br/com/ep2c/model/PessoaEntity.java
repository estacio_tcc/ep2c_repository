package br.com.ep2c.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "pessoa")
@Inheritance(strategy = InheritanceType.JOINED)
public class PessoaEntity implements IEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 6326429805866750045L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "nome", nullable = false)
    private String nome;

    @Column(name = "cpf_cnpj", nullable = false)
    private String cpfCnpj;

    @Column(name = "data_criacao", nullable = false)
    private Date dataCriacao;

    @ManyToOne
    @JoinColumn(name = "endereco", referencedColumnName = "id")
    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    private EnderecoEntity endereco;

    public void setDataCriacao(Date dataCriacao) {
	this.dataCriacao = dataCriacao;
    }

    public Date getDataCriacao() {
	return dataCriacao;
    }

    public String getNome() {
	return nome;
    }

    public void setNome(String nome) {
	this.nome = nome;
    }

    public String getCpfCnpj() {
	return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
	this.cpfCnpj = cpfCnpj;
    }

    public EnderecoEntity getEndereco() {
	return endereco;
    }

    public void setEndereco(EnderecoEntity endereco) {
	this.endereco = endereco;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Override
    public String toString() {
	return this.getClass().getSimpleName() + " - [id=" + id + ", nome=" + nome
		+ ", cpfCnpj=" + cpfCnpj + ", dataCriacao=" + dataCriacao + ", endereco="
		+ endereco + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((cpfCnpj == null) ? 0 : cpfCnpj.hashCode());
	result = prime * result + ((dataCriacao == null) ? 0 : dataCriacao.hashCode());
	result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nome == null) ? 0 : nome.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	PessoaEntity other = (PessoaEntity) obj;
	if (cpfCnpj == null) {
	    if (other.cpfCnpj != null)
		return false;
	} else if (!cpfCnpj.equals(other.cpfCnpj))
	    return false;
	if (dataCriacao == null) {
	    if (other.dataCriacao != null)
		return false;
	} else if (!dataCriacao.equals(other.dataCriacao))
	    return false;
	if (endereco == null) {
	    if (other.endereco != null)
		return false;
	} else if (!endereco.equals(other.endereco))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nome == null) {
	    if (other.nome != null)
		return false;
	} else if (!nome.equals(other.nome))
	    return false;
	return true;
    }

}
