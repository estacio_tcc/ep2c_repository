package br.com.ep2c.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ordem_entrega")
public class OrdemEntregaEntity implements IEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 7890518682104088419L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToMany(mappedBy = "ordensEntrega")
    private List<ItemEntregaEntity> itensEntrega;

    @Column(name = "data_criacao")
    private Date dataCriacao;

    @Column(name = "cliente")
    private ClienteEntity cliente;

    @Column(name = "total_carga")
    private BigDecimal totalCarga;

    @Column(name = "data_saida")
    private Date dataSaida;

    @Column(name = "hora_saida")
    private Integer horaSaida;

    @Column(name = "veiculo")
    private VeiculoEntity veiculo;

    @Column(name = "motorista")
    private FuncionarioEntity motorista;

    @Column(name = "peso_maximo")
    private BigDecimal pesoMaximo;

    @Column(name = "roteiro")
    private String roteiro;

    @Column(name = "transportadora")
    private TransportadoraEntity transportadora;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public List<ItemEntregaEntity> getItensEntrega() {
	return itensEntrega;
    }

    public void setItensEntrega(List<ItemEntregaEntity> itensEntrega) {
	this.itensEntrega = itensEntrega;
    }

    public Date getDataCriacao() {
	return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
	this.dataCriacao = dataCriacao;
    }

    public ClienteEntity getCliente() {
	return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
	this.cliente = cliente;
    }

    public BigDecimal getTotalCarga() {
	return totalCarga;
    }

    public void setTotalCarga(BigDecimal totalCarga) {
	this.totalCarga = totalCarga;
    }

    public Date getDataSaida() {
	return dataSaida;
    }

    public void setDataSaida(Date dataSaida) {
	this.dataSaida = dataSaida;
    }

    public Integer getHoraSaida() {
	return horaSaida;
    }

    public void setHoraSaida(Integer horaSaida) {
	this.horaSaida = horaSaida;
    }

    public VeiculoEntity getVeiculo() {
	return veiculo;
    }

    public void setVeiculo(VeiculoEntity veiculo) {
	this.veiculo = veiculo;
    }

    public FuncionarioEntity getMotorista() {
	return motorista;
    }

    public void setMotorista(FuncionarioEntity motorista) {
	this.motorista = motorista;
    }

    public BigDecimal getPesoMaximo() {
	return pesoMaximo;
    }

    public void setPesoMaximo(BigDecimal pesoMaximo) {
	this.pesoMaximo = pesoMaximo;
    }

    public String getRoteiro() {
	return roteiro;
    }

    public void setRoteiro(String roteiro) {
	this.roteiro = roteiro;
    }

    public TransportadoraEntity getTransportadora() {
	return transportadora;
    }

    public void setTransportadora(TransportadoraEntity transportadora) {
	this.transportadora = transportadora;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	OrdemEntregaEntity other = (OrdemEntregaEntity) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "OrdemEntregaEntity [id=" + id + ", dataCriacao=" + dataCriacao
		+ ", cliente=" + cliente + ", totalCarga=" + totalCarga + ", dataSaida="
		+ dataSaida + ", horaSaida=" + horaSaida + ", veiculo=" + veiculo
		+ ", motorista=" + motorista + ", pesoMaximo=" + pesoMaximo + ", roteiro="
		+ roteiro + ", transportadora=" + transportadora + "]";
    }

}
