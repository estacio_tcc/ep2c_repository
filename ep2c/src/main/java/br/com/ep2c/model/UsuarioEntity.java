package br.com.ep2c.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "usuario")
@NamedQueries({
	@NamedQuery(name = UsuarioEntity.findByLoginFree, query = "select u from UsuarioEntity u where "
		+ "u.login in (select l from LoginEntity l where l.login = :login)") })
public class UsuarioEntity implements IEntity {

    /**
    * 
    */
    private static final long serialVersionUID = 3633085508153087160L;

    public static final String findByLoginFree = "findByLoginFree";

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "funcionario", referencedColumnName = "id")
    @Cascade({ CascadeType.SAVE_UPDATE })
    private FuncionarioEntity funcionario;

    @ManyToOne
    @JoinColumn(name = "login", referencedColumnName = "id")
    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    private LoginEntity login;

    @Column(name = "perfil")
    private Integer perfil;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public FuncionarioEntity getFuncionario() {
	return funcionario;
    }

    public void setFuncionario(FuncionarioEntity funcionario) {
	this.funcionario = funcionario;
    }

    public LoginEntity getLogin() {
	return login;
    }

    public void setLogin(LoginEntity login) {
	this.login = login;
    }

    public Integer getPerfil() {
	return perfil;
    }

    public void setPerfil(Integer perfil) {
	this.perfil = perfil;
    }

    @Override
    public String toString() {
	return "UsuarioEntity [id=" + id + ", funcionario=" + funcionario + ", login="
		+ login + ", perfil=" + perfil + "]";
    }
}
