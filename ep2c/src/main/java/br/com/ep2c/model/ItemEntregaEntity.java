package br.com.ep2c.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "item_entrega")
public class ItemEntregaEntity implements IEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 906380234492804060L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(cascade = javax.persistence.CascadeType.ALL)
    @JoinColumn(name = "pedido", referencedColumnName = "id")
    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    private PedidoEntity pedido;

    /**
     * Consulta
     * http://www.guj.com.br/t/relacionamento-manytomany-hibernate/260516/7
     */
    @ManyToMany
    @JoinTable(name = "item_entrega_ordem_entrega", joinColumns = @JoinColumn(name = "ie_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "oe_id", referencedColumnName = "id"))
    private List<OrdemEntregaEntity> ordensEntrega;

    public Integer getId() {
	return id;
    }

    public PedidoEntity getPedido() {
	return pedido;
    }

    public void setPedido(PedidoEntity pedido) {
	this.pedido = pedido;
    }

    public List<OrdemEntregaEntity> getOrdensEntrega() {
	return ordensEntrega;
    }

    public void setOrdensEntrega(List<OrdemEntregaEntity> ordensEntrega) {
	this.ordensEntrega = ordensEntrega;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ItemEntregaEntity other = (ItemEntregaEntity) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}
