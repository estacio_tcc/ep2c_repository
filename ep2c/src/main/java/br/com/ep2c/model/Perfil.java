package br.com.ep2c.model;

public enum Perfil {

    Admin("Administrador", Integer.valueOf(0)), UserSystem("Usuário do Sistema", Integer.valueOf(1)), UserFunc(
	    "Funcionário", Integer.valueOf(2)), UserMotor("Motorista",
		    Integer.valueOf(3)), UserKeyRing("Chaveiro", Integer.valueOf(4));

    private final String nome;
    private final Integer codigo;

    private Perfil(String nome, Integer codigo) {
	this.nome = nome;
	this.codigo = codigo;
    }

    public String getNome() {
	return nome;
    }

    public Integer getCodigo() {
	return codigo;
    }
}
