package br.com.ep2c.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "cliente")
@PrimaryKeyJoinColumn(name = "id")
public class ClienteEntity extends PessoaEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 6326429805866750045L;

    @Column(name = "razao_social", nullable = false)
    private String razaoSocial;

    @Column(name = "insc_estadual", nullable = false)
    private String inscEstadual;

    public String getRazaoSocial() {
	return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
	this.razaoSocial = razaoSocial;
    }

    public String getInscEstadual() {
	return inscEstadual;
    }

    public void setInscEstadual(String inscEstadual) {
	this.inscEstadual = inscEstadual;
    }

    @Override
    public String toString() {
	return super.toString() + "[razaoSocial=" + razaoSocial + ", inscEstadual="
		+ inscEstadual + "]";
    }
}
