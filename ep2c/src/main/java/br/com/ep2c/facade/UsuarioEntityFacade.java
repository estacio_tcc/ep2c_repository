package br.com.ep2c.facade;

import br.com.ep2c.dao.UsuarioEntityDAO;
import br.com.ep2c.model.LoginEntity;
import br.com.ep2c.model.UsuarioEntity;

public class UsuarioEntityFacade
	extends AbstractEntityFacade<UsuarioEntity, UsuarioEntityDAO> {

    /**
     * 
     */
    private static final long serialVersionUID = -6059364095410440L;

    @Override
    protected UsuarioEntityDAO getEntityDAO() {
	return new UsuarioEntityDAO();
    }

    public UsuarioEntity getUsuarioByPerfil(LoginEntity login) {
	return getEntityDAO().getUsuarioByPerfil(login);
    }
}
