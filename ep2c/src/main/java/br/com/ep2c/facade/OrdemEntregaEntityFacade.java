package br.com.ep2c.facade;

import br.com.ep2c.dao.OrdemEntregaEntityDAO;
import br.com.ep2c.model.OrdemEntregaEntity;

public class OrdemEntregaEntityFacade
	extends AbstractEntityFacade<OrdemEntregaEntity, OrdemEntregaEntityDAO> {

    /**
     * 
     */
    private static final long serialVersionUID = 7287060211064648450L;

    @Override
    protected OrdemEntregaEntityDAO getEntityDAO() {
	return new OrdemEntregaEntityDAO();
    }

}
