package br.com.ep2c.facade;

import br.com.ep2c.dao.ClienteEntityDAO;
import br.com.ep2c.model.ClienteEntity;

public class ClienteEntityFacade
	extends AbstractEntityFacade<ClienteEntity, ClienteEntityDAO> {

    /**
     * 
     */
    private static final long serialVersionUID = 5265592857588080234L;

    @Override
    protected ClienteEntityDAO getEntityDAO() {
	return new ClienteEntityDAO();
    }

}
