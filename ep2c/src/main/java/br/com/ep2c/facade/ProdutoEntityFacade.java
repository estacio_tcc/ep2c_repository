package br.com.ep2c.facade;

import java.util.List;

import br.com.ep2c.dao.ProdutoEntityDAO;
import br.com.ep2c.model.ProdutoEntity;

public class ProdutoEntityFacade
	extends AbstractEntityFacade<ProdutoEntity, ProdutoEntityDAO> {

    /**
     * 
     */
    private static final long serialVersionUID = 5265592857588080234L;

    @Override
    protected ProdutoEntityDAO getEntityDAO() {
	return new ProdutoEntityDAO();
    }

    public List<String> findByLikeNome(String nome) {
	return getEntityDAO().findByLikeNome(nome);
    }

    public ProdutoEntity findByNome(String nome) {
	return getEntityDAO().findByNome(nome);
    }
}
