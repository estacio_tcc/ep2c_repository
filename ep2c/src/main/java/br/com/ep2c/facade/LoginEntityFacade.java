package br.com.ep2c.facade;

import br.com.ep2c.dao.LoginEntityDAO;
import br.com.ep2c.model.LoginEntity;

public class LoginEntityFacade extends AbstractEntityFacade<LoginEntity, LoginEntityDAO> {

    /**
     * 
     */
    private static final long serialVersionUID = -6059364095410440L;

    @Override
    protected LoginEntityDAO getEntityDAO() {
	return new LoginEntityDAO();
    }

    public Boolean isValid(String login) {
	return getEntityDAO().isValid(login);
    }

}
