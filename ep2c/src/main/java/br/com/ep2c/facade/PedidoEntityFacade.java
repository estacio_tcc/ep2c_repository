package br.com.ep2c.facade;

import br.com.ep2c.dao.PedidoEntityDAO;
import br.com.ep2c.model.PedidoEntity;

public class PedidoEntityFacade
	extends AbstractEntityFacade<PedidoEntity, PedidoEntityDAO> {

    /**
     * 
     */
    private static final long serialVersionUID = 7128792357590327655L;

    @Override
    protected PedidoEntityDAO getEntityDAO() {
	return new PedidoEntityDAO();
    }
}
