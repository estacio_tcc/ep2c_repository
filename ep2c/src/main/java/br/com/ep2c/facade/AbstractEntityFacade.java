package br.com.ep2c.facade;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;

import br.com.ep2c.dao.AbstractEntityDAO;
import br.com.ep2c.model.IEntity;

public abstract class AbstractEntityFacade<E extends IEntity, D extends AbstractEntityDAO<E>>
	implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2095540030655159812L;

    protected abstract D getEntityDAO();

    public void save(E obj) {
	getEntityDAO().save(obj);
    }

    public void update(E obj) {
	getEntityDAO().update(obj);
    }

    public void saveOrUpdate(E obj) {
	if (obj.getId() == null) {
	    getEntityDAO().save(obj);
	} else {
	    getEntityDAO().update(obj);
	}
    }

    public E findById(Integer id) {
	return getEntityDAO().findById(id);
    }

    public void delete(E obj) {
	getEntityDAO().delete(obj);
    }

    public void delete(Object obj) {
	getEntityDAO().delete(obj);
    }

    public boolean exists(E obj) {
	return getEntityDAO().exists(obj);
    }

    public E search(E obj) throws RuntimeException {
	return getEntityDAO().search(obj);
    }

    public List<E> list(String... attributesAndOrder) {
	return getEntityDAO().list(attributesAndOrder);
    }

    public boolean existsResult(Criteria criteria) {
	return getEntityDAO().existsResult(criteria);
    }

    public List<E> searchByField(String fieldName, Object fieldValue,
	    String... attributesAndOrder) {
	return getEntityDAO().searchByField(fieldName, fieldValue, attributesAndOrder);
    }

    public E searchOneByField(String fieldName, Object fieldValue) {
	return getEntityDAO().searchOneByField(fieldName, fieldValue);
    }

    public E searchOneByField(String fieldName, Object fieldValue, Class<E> entityClass) {
	return getEntityDAO().searchOneByField(fieldName, fieldValue, entityClass);
    }
}
