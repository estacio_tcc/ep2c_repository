package br.com.ep2c.facade;

import br.com.ep2c.dao.ItemPedidoEntityDAO;
import br.com.ep2c.model.ItemPedidoEntity;

public class ItemPedidoEntityFacade
	extends AbstractEntityFacade<ItemPedidoEntity, ItemPedidoEntityDAO> {

    /**
     * 
     */
    private static final long serialVersionUID = 2908318220452330343L;

    @Override
    protected ItemPedidoEntityDAO getEntityDAO() {
	return new ItemPedidoEntityDAO();
    }
}
