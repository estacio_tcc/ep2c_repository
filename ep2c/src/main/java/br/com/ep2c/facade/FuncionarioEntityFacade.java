package br.com.ep2c.facade;

import java.util.List;

import br.com.ep2c.dao.FuncionarioEntityDAO;
import br.com.ep2c.model.FuncionarioEntity;

public class FuncionarioEntityFacade
	extends AbstractEntityFacade<FuncionarioEntity, FuncionarioEntityDAO> {

    /**
     * 
     */
    private static final long serialVersionUID = 5265592857588080234L;

    @Override
    protected FuncionarioEntityDAO getEntityDAO() {
	return new FuncionarioEntityDAO();
    }

    public List<FuncionarioEntity> listByNotUsed() {
	return getEntityDAO().listByNotUsed();
    }
}
