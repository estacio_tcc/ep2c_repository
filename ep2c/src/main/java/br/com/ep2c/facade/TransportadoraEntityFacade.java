package br.com.ep2c.facade;

import br.com.ep2c.dao.TransportadoraEntityDAO;
import br.com.ep2c.model.TransportadoraEntity;

public class TransportadoraEntityFacade
	extends AbstractEntityFacade<TransportadoraEntity, TransportadoraEntityDAO> {

    /**
     * 
     */
    private static final long serialVersionUID = -884682845082480447L;

    @Override
    protected TransportadoraEntityDAO getEntityDAO() {
	return new TransportadoraEntityDAO();
    }
}
