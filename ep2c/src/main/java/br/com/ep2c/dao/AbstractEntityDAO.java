package br.com.ep2c.dao;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.ep2c.model.IEntity;
import br.com.ep2c.util.HibernateUtil;

public abstract class AbstractEntityDAO<E extends IEntity>
	implements Serializable, IEntityDAO<E> {

    /**
     * 
     */
    private static final long serialVersionUID = 1194790920222283836L;

    private static final Logger log = Logger.getLogger(AbstractEntityDAO.class);

    protected abstract Class<E> getPersistedClass();

    private Session session;
    private Transaction transaction;

    public Integer save(E obj) {
	log.debug("No metodo save[" + obj + "]");
	Integer id = -1;
	try {
	    this.session = getCurrentSession();
	    this.transaction = session.beginTransaction();
	    Serializable s = this.session.save(obj);
	    log.debug("Serializable[" + s + "]");
	    this.transaction.commit();
	} catch (HibernateException e) {
	    log.error("Ocorreu um erro ao salvar o objeto", e);
	    throw new RuntimeException("Ocorreu um erro ao salvar o objeto", e);
	} finally {
	    try {
		if (this.session.isOpen()) {
		    this.session.close();
		}
	    } catch (Throwable e) {
		log.error("Ocorreu um erro ao fechar conexao", e);
		throw new RuntimeException("Ocorreu um erro ao salvar o objeto", e);
	    }
	}
	return id;
    }

    public void update(E obj) {
	log.debug("No metodo update[" + obj + "]");
	try {
	    this.session = getCurrentSession();
	    this.transaction = this.session.beginTransaction();
	    this.session.update(obj);
	    this.transaction.commit();
	} catch (HibernateException e) {
	    log.error("Ocorreu um erro ao atualizar o objeto", e);
	    throw new RuntimeException("Ocorreu um erro ao salvar o objeto", e);
	} finally {
	    try {
		if (this.session.isOpen()) {
		    this.session.close();
		}
	    } catch (Throwable e) {
		log.error("Ocorreu um erro ao fechar conexao", e);
		throw new RuntimeException("Ocorreu um erro ao salvar o objeto", e);
	    }
	}
    }

    public void delete(E obj) {
	log.debug("No metodo delete[" + obj + "]");
	try {
	    this.session = getCurrentSession();
	    this.transaction = this.session.beginTransaction();
	    this.session.delete(obj);
	    this.transaction.commit();
	} catch (HibernateException e) {
	    log.error("Ocorreu um erro ao deletar o objeto", e);
	    throw new RuntimeException("Ocorreu um erro ao salvar o objeto", e);
	} finally {
	    try {
		if (this.session.isOpen()) {
		    this.session.close();
		}
	    } catch (Throwable e) {
		log.error("Ocorreu um erro ao fechar conexao", e);
		throw new RuntimeException("Ocorreu um erro ao salvar o objeto", e);
	    }
	}
    }

    @SuppressWarnings("unchecked")
    public E findById(Integer id) {
	log.debug("No metodo findById[" + id + "]");
	StringBuilder query = new StringBuilder("select e from ");
	query.append(getPersistedClass().getSimpleName());
	query.append(" e where e.id = :id");
	Session session = HibernateUtil.getSession();
	session.beginTransaction();
	E entity = (E) session.getNamedQuery(query.toString()).setParameter("id", id)
		.uniqueResult();
	session.close();
	return entity;
    }

    public void delete(Object obj) {
	log.debug("No metodo delete[" + obj + "]");
	getCurrentSession().delete(obj);
    }

    public boolean exists(E obj) {
	log.debug("No metodo exists[" + obj + "]");
	Criteria criteria = createCriteria(getPersistedClass());
	criteria.add(Restrictions.idEq(obj.getId()));
	return existsResult(criteria);
    }

    public E search(E obj) throws RuntimeException {
	log.debug("No metodo search[" + obj + "]");
	return getPersistedClass()
		.cast(getCurrentSession().get(getPersistedClass(), obj.getId()));
    }

    @SuppressWarnings("unchecked")
    public List<E> list(String... attributesAndOrder) {
	log.debug("No metodo list[" + attributesAndOrder + "]");
	Criteria criteria = createCriteria();

	if (attributesAndOrder.length > 0) {
	    Map<String, Boolean> mapOrder = createMapOrder(attributesAndOrder);
	    addOrdenation(criteria, mapOrder);
	}

	return Collections.checkedList(criteria.list(), getPersistedClass());
    }

    protected Criteria createCriteria() {
	log.debug("No metodo createCriteria");
	Session currentSession = getCurrentSession();
	return currentSession.createCriteria(getPersistedClass());
    }

    protected Query createQuery(String hql) {
	log.debug("No metodo createQuery[" + hql + "]");
	Session currentSession = getCurrentSession();
	return currentSession.createQuery(hql);
    }

    protected Criteria createCriteria(Class<?> objectClass) {
	return getCurrentSession().createCriteria(objectClass);
    }

    protected void addCriteriaOrder(String property, boolean upward, Criteria criteria) {
	if (upward) {
	    criteria.addOrder(Order.asc(property).ignoreCase());
	} else {
	    criteria.addOrder(Order.desc(property).ignoreCase());
	}
    }

    protected void addOrdenation(Criteria criteria, Map<String, Boolean> property) {

	if (property != null) {
	    for (Entry<String, Boolean> entry : property.entrySet()) {
		addCriteriaOrder(entry.getKey(), entry.getValue(), criteria);
	    }
	}
    }

    public Session getCurrentSession() {
	return HibernateUtil.getSession();
    }

    protected Long getTotalRecords(Criteria criteria) {
	criteria.setProjection(Projections.rowCount());
	return (Long) criteria.uniqueResult();
    }

    public boolean existsResult(Criteria criteria) {
	return getTotalRecords(criteria) > 0;
    }

    @SuppressWarnings("unchecked")
    public List<E> searchByField(String fieldName, Object fieldValue,
	    String... attributesAndOrder) {
	Criteria criteria = createCriteria(getPersistedClass());

	if (attributesAndOrder.length > 0) {
	    Map<String, Boolean> ordemMap = createMapOrder(attributesAndOrder);
	    addOrdenation(criteria, ordemMap, new HashSet<String>());
	}

	if (fieldValue instanceof String) {
	    criteria.add(Restrictions.eq(fieldName, fieldValue).ignoreCase());
	} else {
	    criteria.add(Restrictions.eq(fieldName, fieldValue));
	}
	return criteria.list();
    }

    public E searchOneByField(String fieldName, Object fieldValue) {
	return searchOneByField(fieldName, fieldValue, getPersistedClass());
    }

    @SuppressWarnings("unchecked")
    public E searchOneByField(String fieldName, Object fieldValue, Class<E> entityClass) {
	Criteria criteria = createCriteria(entityClass);

	if (fieldValue instanceof String) {
	    criteria.add(Restrictions.eq(fieldName, fieldValue).ignoreCase());
	} else {
	    criteria.add(Restrictions.eq(fieldName, fieldValue));
	}

	criteria.setMaxResults(1);
	return (E) criteria.uniqueResult();
    }

    private Map<String, Boolean> createMapOrder(String... attributesAndOrder) {
	Map<String, Boolean> orderMap = new HashMap<String, Boolean>();

	for (String attributeAndOrder : attributesAndOrder) {
	    String value;
	    String attribute;
	    boolean order;
	    int tokenIndex = attributeAndOrder.indexOf(':');

	    if (tokenIndex > 0) {
		attribute = attributeAndOrder.substring(0, tokenIndex);
		value = attributeAndOrder.substring(tokenIndex + 1,
			attributeAndOrder.length());
	    } else {
		attribute = attributeAndOrder;
		value = "asc";
	    }
	    if ("asc".equalsIgnoreCase(value)) {
		order = true;
	    } else if ("desc".equalsIgnoreCase(value)) {
		order = false;
	    } else {
		throw new IllegalArgumentException(
			"Invalid attribute ordering syntax: " + attributeAndOrder);
	    }
	    orderMap.put(attribute, order);
	}
	return orderMap;
    }

    @SuppressWarnings("deprecation")
    protected void addOrdenation(Criteria criteria, Map<String, Boolean> order,
	    Set<String> aliases) {

	for (Entry<String, Boolean> entry : order.entrySet()) {

	    String attribute = entry.getKey();
	    Boolean upward = entry.getValue();

	    String alias = getAlias(attribute);
	    if (StringUtils.isNotBlank(alias) && !aliases.contains(alias)) {
		aliases.add(alias);
		criteria.createAlias(alias, alias, CriteriaSpecification.LEFT_JOIN);
	    }

	    if (upward) {
		criteria.addOrder(Order.asc(attribute).ignoreCase());
	    } else {
		criteria.addOrder(Order.desc(attribute).ignoreCase());
	    }
	}
    }

    private String getAlias(String attribute) {
	String alias = null;
	int index = attribute.indexOf('.');

	if (index > 0) {
	    alias = attribute.substring(0, index);
	}
	return alias;
    }
}
