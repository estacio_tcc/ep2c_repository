package br.com.ep2c.dao;

import br.com.ep2c.model.ItemPedidoEntity;

public class ItemPedidoEntityDAO extends AbstractEntityDAO<ItemPedidoEntity> {

    /**
     * 
     */
    private static final long serialVersionUID = -4878334704054685060L;

    @Override
    protected Class<ItemPedidoEntity> getPersistedClass() {
	return ItemPedidoEntity.class;
    }

}
