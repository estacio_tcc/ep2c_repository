package br.com.ep2c.dao;

import br.com.ep2c.model.TransportadoraEntity;

public class TransportadoraEntityDAO extends AbstractEntityDAO<TransportadoraEntity> {

    /**
     * 
     */
    private static final long serialVersionUID = -641553496772491025L;

    @Override
    protected Class<TransportadoraEntity> getPersistedClass() {
	return TransportadoraEntity.class;
    }

}
