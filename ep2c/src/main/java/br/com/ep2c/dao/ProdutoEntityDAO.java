package br.com.ep2c.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import br.com.ep2c.model.ProdutoEntity;
import br.com.ep2c.util.HibernateUtil;

public class ProdutoEntityDAO extends AbstractEntityDAO<ProdutoEntity> {

    /**
     * 
     */
    private static final long serialVersionUID = 5306003214867695713L;

    @Override
    protected Class<ProdutoEntity> getPersistedClass() {
	return ProdutoEntity.class;
    }

    @SuppressWarnings("unchecked")
    public List<String> findByLikeNome(String nome) {
	List<String> result;
	Session session = HibernateUtil.getSession();
	session.beginTransaction();
	result = session.getNamedQuery(ProdutoEntity.findNamesByLikeName)
		.setParameter("nome", nome + "%").list();
	session.close();
	return result == null ? new ArrayList<String>() : result;
    }

    public ProdutoEntity findByNome(String nome) {
	Session session = HibernateUtil.getSession();
	session.beginTransaction();
	ProdutoEntity result = (ProdutoEntity) session
		.getNamedQuery(ProdutoEntity.findByName).setParameter("nome", nome)
		.uniqueResult();
	session.close();
	return result;
    }
}
