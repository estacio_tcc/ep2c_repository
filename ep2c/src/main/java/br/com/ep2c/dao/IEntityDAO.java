package br.com.ep2c.dao;

import br.com.ep2c.model.IEntity;

public interface IEntityDAO<T extends IEntity> {

    public Integer save(T obj);

    public void update(T obj);

    public void delete(T obj);
}
