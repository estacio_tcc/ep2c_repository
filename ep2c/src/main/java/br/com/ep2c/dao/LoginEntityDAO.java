package br.com.ep2c.dao;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.ep2c.model.LoginEntity;
import br.com.ep2c.util.HibernateUtil;

public class LoginEntityDAO extends AbstractEntityDAO<LoginEntity> {

    /**
     * 
     */
    private static final long serialVersionUID = -3545440102622782814L;

    private static Logger log = Logger.getLogger(LoginEntityDAO.class);

    @Override
    protected Class<LoginEntity> getPersistedClass() {
	return LoginEntity.class;
    }

    @Override
    public LoginEntity search(LoginEntity obj) {
	log.debug("No metodo search");
	Session session = HibernateUtil.getSession();
	Criteria criteria = session.createCriteria(LoginEntity.class, "l");
	criteria.add(Restrictions.eq("l.login", obj.getLogin()));
	criteria.add(Restrictions.eq("l.password", obj.getPassword()));

	@SuppressWarnings("unchecked")
	List<LoginEntity> list = Collections.checkedList(criteria.list(),
		LoginEntity.class);
	if (CollectionUtils.isNotEmpty(list)) {
	    return list.get(0);
	}
	log.debug("Nao foi encontrado Login");
	return null;
    }

    public Boolean isValid(String login) {
	log.debug("No metodo isValid[" + login + "]");
	Session session = HibernateUtil.getSession();
	Criteria criteria = session.createCriteria(LoginEntity.class, "l");
	criteria.add(Restrictions.eq("l.login", login));
	@SuppressWarnings("unchecked")
	List<LoginEntity> list = Collections.checkedList(criteria.list(),
		LoginEntity.class);
	if (CollectionUtils.isNotEmpty(list)) {
	    return list.size() == 0;
	}
	log.debug("Login is valid");
	return true;
    }

}
