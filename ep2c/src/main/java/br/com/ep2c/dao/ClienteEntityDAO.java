package br.com.ep2c.dao;

import br.com.ep2c.model.ClienteEntity;

public class ClienteEntityDAO extends AbstractEntityDAO<ClienteEntity> {

    /**
     * 
     */
    private static final long serialVersionUID = 5306003214867695713L;

    @Override
    protected Class<ClienteEntity> getPersistedClass() {
	return ClienteEntity.class;
    }

}
