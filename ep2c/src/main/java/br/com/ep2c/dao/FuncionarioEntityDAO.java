package br.com.ep2c.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import br.com.ep2c.model.FuncionarioEntity;
import br.com.ep2c.util.HibernateUtil;

public class FuncionarioEntityDAO extends AbstractEntityDAO<FuncionarioEntity> {

    /**
     * 
     */
    private static final long serialVersionUID = 5306003214867695713L;

    @Override
    protected Class<FuncionarioEntity> getPersistedClass() {
	return FuncionarioEntity.class;
    }

    @SuppressWarnings("unchecked")
    public List<FuncionarioEntity> listByNotUsed() {
	List<FuncionarioEntity> result;
	Session session = HibernateUtil.getSession();
	result = session.getNamedQuery(FuncionarioEntity.findByNotUsed).list();
	return result == null ? new ArrayList<FuncionarioEntity>() : result;
    }
}
