package br.com.ep2c.dao;

import org.hibernate.Session;

import br.com.ep2c.model.LoginEntity;
import br.com.ep2c.model.UsuarioEntity;
import br.com.ep2c.util.HibernateUtil;

public class UsuarioEntityDAO extends AbstractEntityDAO<UsuarioEntity> {

    /**
     * 
     */
    private static final long serialVersionUID = -3545440102622782814L;

    @Override
    protected Class<UsuarioEntity> getPersistedClass() {
	return UsuarioEntity.class;
    }

    public UsuarioEntity getUsuarioByPerfil(LoginEntity login) {
	UsuarioEntity result;
	Session session = HibernateUtil.getSession();
	result = (UsuarioEntity) session.getNamedQuery(UsuarioEntity.findByLoginFree)
		.setParameter("login", login.getLogin()).uniqueResult();
	return result == null ? new UsuarioEntity() : result;
    }
}
