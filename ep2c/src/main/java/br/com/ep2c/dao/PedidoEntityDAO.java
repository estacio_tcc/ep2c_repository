package br.com.ep2c.dao;

import br.com.ep2c.model.PedidoEntity;

public class PedidoEntityDAO extends AbstractEntityDAO<PedidoEntity> {

    /**
     * 
     */
    private static final long serialVersionUID = -2105761085923683889L;

    @Override
    protected Class<PedidoEntity> getPersistedClass() {
	return PedidoEntity.class;
    }

}
