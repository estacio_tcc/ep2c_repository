package br.com.ep2c.dao;

import br.com.ep2c.model.OrdemEntregaEntity;

public class OrdemEntregaEntityDAO extends AbstractEntityDAO<OrdemEntregaEntity> {

    /**
     * 
     */
    private static final long serialVersionUID = -1251429566891627270L;

    @Override
    protected Class<OrdemEntregaEntity> getPersistedClass() {
	return OrdemEntregaEntity.class;
    }

}
