package br.com.ep2c.view.managedbean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import br.com.ep2c.facade.ProdutoEntityFacade;
import br.com.ep2c.model.ProdutoEntity;

@ManagedBean
@ViewScoped
public class ConsultaExcluiProdutoMB {

    private static final Logger log = Logger.getLogger(ConsultaExcluiProdutoMB.class);

    private List<ProdutoEntity> list;
    private ProdutoEntityFacade facade;
    private ProdutoEntity tmp;

    private ProdutoEntityFacade getFacade() {
	if (facade == null) {
	    facade = new ProdutoEntityFacade();
	}
	return facade;
    }

    @PostConstruct
    public void initialize() {
	log.debug("No metodo initialize");
	list = getFacade().list();
    }

    public List<ProdutoEntity> getList() {
	return list;
    }

    public void setList(List<ProdutoEntity> list) {
	this.list = list;
    }

    public String remove() {
	log.debug("No metodo remove");
	try {
	    log.debug("ProdutoEntity[" + tmp + "]");
	    getFacade().delete(tmp);
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    "Erro ao salvar cliente", "Erro cliente"));
	    e.printStackTrace();
	}
	return "/produto/consultaExcluiProduto.xhtml";
    }

    public String edit() {
	log.debug("No metodo edit");
	try {
	    log.debug("ProdutoEntity[" + tmp + "]");

	    FacesContext.getCurrentInstance().getExternalContext().getFlash()
		    .put("entity", tmp);

	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    "Erro ao salvar cliente", "Erro cliente"));
	    e.printStackTrace();
	    return "/produto/consultaExcluiProduto.xhtml";
	}
	return "/produto/inseriAtualizaProduto.xhtml";
    }

    public ProdutoEntity getTmp() {
	return tmp;
    }

    public void setTmp(ProdutoEntity tmp) {
	this.tmp = tmp;
    }
}
