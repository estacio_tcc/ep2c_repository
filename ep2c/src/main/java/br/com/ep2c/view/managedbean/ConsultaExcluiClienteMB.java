package br.com.ep2c.view.managedbean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import br.com.ep2c.facade.ClienteEntityFacade;
import br.com.ep2c.model.ClienteEntity;

@ManagedBean
@ViewScoped
public class ConsultaExcluiClienteMB {

    private static final Logger log = Logger.getLogger(ConsultaExcluiClienteMB.class);

    private List<ClienteEntity> list;
    private ClienteEntityFacade facade;
    private ClienteEntity tmp;

    private ClienteEntityFacade getFacade() {
	if (facade == null) {
	    facade = new ClienteEntityFacade();
	}
	return facade;
    }

    @PostConstruct
    public void initialize() {
	log.debug("No metodo initialize");
	list = getFacade().list();
    }

    public List<ClienteEntity> getList() {
	return list;
    }

    public void setList(List<ClienteEntity> list) {
	this.list = list;
    }

    public String remove() {
	log.debug("No metodo edit");
	try {
	    log.debug("ClienteEntity[" + tmp + "]");
	    getFacade().delete(tmp);
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    "Erro ao remover cliente", "Erro cliente"));
	    e.printStackTrace();
	}
	return "/cliente/consultaExcluiCliente.xhtml";
    }

    public String edit() {
	log.debug("No metodo edit");
	try {
	    log.debug("ClienteEntity[" + tmp + "]");

	    FacesContext.getCurrentInstance().getExternalContext().getFlash()
		    .put("cliente", tmp);

	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    "Erro ao salvar cliente", "Erro cliente"));
	    e.printStackTrace();
	    return "/cliente/consultaExcluiCliente.xhtml";
	}
	return "/cliente/inseriAtualizaCliente.xhtml";
    }

    public ClienteEntity getTmp() {
	return tmp;
    }

    public void setTmp(ClienteEntity tmp) {
	this.tmp = tmp;
    }
}
