package br.com.ep2c.view.managedbean;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import br.com.ep2c.facade.LoginEntityFacade;
import br.com.ep2c.model.LoginEntity;
import br.com.ep2c.util.PasswdUtil;

@ManagedBean
@SessionScoped
public class LoginMB implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1574919551511472365L;

    private static final Logger log = Logger.getLogger(LoginMB.class);

    private LoginEntityFacade facade;

    private LoginEntity login = new LoginEntity();

    public LoginEntityFacade getFacade() {
	if (facade == null) {
	    facade = new LoginEntityFacade();
	}
	return facade;
    }

    public LoginEntity getLogin() {
	return login;
    }

    public void setLogin(LoginEntity login) {
	this.login = login;
    }

    public String login() {
	log.debug("No metodo login");

	String passwordEncriptado = PasswdUtil.encript(login.getPassword());
	// Clonado para não ter a mesma referência do objeto
	LoginEntity tmp = login.clone();
	tmp.setPassword(passwordEncriptado);

	tmp = getFacade().search(tmp);
	if (tmp == null) {

	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário não encontrado!", "Erro no Login!"));
	    return "index.xhtml";
	}

	HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
		.getRequest();
	request.setAttribute("LoginEntity", tmp);
	this.login = tmp;

	return "/home/home.xhtml";
    }

    public String logout() {
	log.debug("No metodo logout");
	login = new LoginEntity();
	return "/ep2c/index.xhtml";
    }
}
