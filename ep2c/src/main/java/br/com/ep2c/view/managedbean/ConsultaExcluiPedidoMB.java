package br.com.ep2c.view.managedbean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import br.com.ep2c.facade.PedidoEntityFacade;
import br.com.ep2c.model.PedidoEntity;

@ManagedBean
@ViewScoped
public class ConsultaExcluiPedidoMB {

    private static final Logger log = Logger.getLogger(ConsultaExcluiPedidoMB.class);

    private List<PedidoEntity> list;
    private PedidoEntityFacade facade;
    private PedidoEntity tmp;

    private PedidoEntityFacade getFacade() {
	if (facade == null) {
	    facade = new PedidoEntityFacade();
	}
	return facade;
    }

    @PostConstruct
    public void initialize() {
	log.debug("No metodo initialize");
	list = getFacade().list();
    }

    public List<PedidoEntity> getList() {
	return list;
    }

    public void setList(List<PedidoEntity> list) {
	this.list = list;
    }

    public String remove() {
	log.debug("No metodo edit");
	try {
	    log.debug("PedidoEntity[" + tmp + "]");
	    getFacade().delete(tmp);
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	}
	return "/pedido/consultaExcluiPedido.xhtml";
    }

    public String edit() {
	log.debug("No metodo edit");
	try {
	    log.debug("PedidoEntity[" + tmp + "]");

	    FacesContext.getCurrentInstance().getExternalContext().getFlash()
		    .put("entity", tmp);

	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	    return "/pedido/consultaExcluiPedido.xhtml";
	}
	return "/pedido/inseriAtualizaPedido.xhtml";
    }

    public PedidoEntity getTmp() {
	return tmp;
    }

    public void setTmp(PedidoEntity tmp) {
	this.tmp = tmp;
    }
}
