package br.com.ep2c.view.managedbean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import br.com.ep2c.facade.FuncionarioEntityFacade;
import br.com.ep2c.model.FuncionarioEntity;

@ManagedBean
@ViewScoped
public class ConsultaExcluiFuncionarioMB {

    private static final Logger log = Logger.getLogger(ConsultaExcluiFuncionarioMB.class);

    private List<FuncionarioEntity> list;
    private FuncionarioEntityFacade facade;
    private FuncionarioEntity tmp;

    private FuncionarioEntityFacade getFacade() {
	if (facade == null) {
	    facade = new FuncionarioEntityFacade();
	}
	return facade;
    }

    @PostConstruct
    public void initialize() {
	log.debug("No metodo initialize");
	list = getFacade().list();
    }

    public List<FuncionarioEntity> getList() {
	return list;
    }

    public void setList(List<FuncionarioEntity> list) {
	this.list = list;
    }

    public String remove() {
	log.debug("No metodo edit");
	try {
	    log.debug("FuncionarioEntity[" + tmp + "]");
	    getFacade().delete(tmp);
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	}
	return "/funcionario/consultaExcluiFuncionario.xhtml";
    }

    public String edit() {
	log.debug("No metodo edit");
	try {
	    log.debug("FuncionarioEntity[" + tmp + "]");

	    FacesContext.getCurrentInstance().getExternalContext().getFlash()
		    .put("entity", tmp);

	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	    return "/funcionario/consultaExcluiFuncionario.xhtml";
	}
	return "/funcionario/inseriAtualizaFuncionario.xhtml";
    }

    public FuncionarioEntity getTmp() {
	return tmp;
    }

    public void setTmp(FuncionarioEntity tmp) {
	this.tmp = tmp;
    }
}
