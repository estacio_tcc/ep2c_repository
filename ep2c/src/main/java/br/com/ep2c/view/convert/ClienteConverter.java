package br.com.ep2c.view.convert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.convert.FacesConverter;

import br.com.ep2c.facade.ClienteEntityFacade;
import br.com.ep2c.model.ClienteEntity;

@FacesConverter("clienteConverter")
public class ClienteConverter extends PessoaConverter<ClienteEntity> {

    private Map<String, ClienteEntity> map;

    private ClienteEntityFacade facade;

    @Override
    protected Object getObject() {
	return new ClienteEntity();
    }

    public Map<String, ClienteEntity> getMap() {
	if (map == null || map.size() == 0) {
	    map = new HashMap<String, ClienteEntity>();

	    List<ClienteEntity> list = getFacade().list();
	    for (ClienteEntity f : list) {
		map.put(f.getNome(), f);
	    }
	}
	return map;
    }

    private ClienteEntityFacade getFacade() {
	if (facade == null) {
	    facade = new ClienteEntityFacade();
	}
	return facade;
    }

}
