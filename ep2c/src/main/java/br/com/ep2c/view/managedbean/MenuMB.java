package br.com.ep2c.view.managedbean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import br.com.ep2c.model.LoginEntity;
import br.com.ep2c.util.Constants;

@ManagedBean
@SessionScoped
public class MenuMB {

    private static final Logger log = Logger.getLogger(MenuMB.class);

    public void logout() {
	log.debug("No metodo logout");
	try {
	    HttpServletRequest rq = (HttpServletRequest) FacesContext.getCurrentInstance()
		    .getExternalContext().getRequest();

	    LoginMB mb = (LoginMB) rq.getSession().getAttribute(Constants.LOGIN_MB);
	    mb.setLogin(new LoginEntity());
	    rq.getSession().setAttribute(Constants.LOGIN_MB, mb);

	    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	    ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
	} catch (Exception e) {
	    log.error("Erro ao efetuar logout");
	    e.printStackTrace();
	}
    }
}
