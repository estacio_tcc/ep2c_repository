package br.com.ep2c.view.managedbean;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;

import br.com.ep2c.facade.ProdutoEntityFacade;
import br.com.ep2c.model.ProdutoEntity;

@ManagedBean
@ViewScoped
public class InseriAtualizaProdutoMB implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2652161360363895079L;

    private ProdutoEntity entity;
    private ProdutoEntityFacade facade;

    @PostConstruct
    public void loadEntity() {

	ProdutoEntity p = (ProdutoEntity) FacesContext.getCurrentInstance()
		.getExternalContext().getFlash().get("entity");

	if (p == null) {
	    p = new ProdutoEntity();
	}
	this.entity = p;
    }

    public ProdutoEntityFacade getFacade() {
	if (facade == null) {
	    facade = new ProdutoEntityFacade();
	}
	return facade;
    }

    public ProdutoEntity getEntity() {
	return entity;
    }

    public void setEntity(ProdutoEntity entity) {
	this.entity = entity;
    }

    public String salvar() {

	try {
	    StringBuilder messageError = new StringBuilder();
	    if (isValid(messageError)) {
		getFacade().saveOrUpdate(entity);
	    } else {
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_ERROR,
				messageError.toString(), "Erro entity"));
		return "/produto/inseriAtualizaProduto.xhtml";
	    }
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	    return "/produto/inseriAtualizaProduto.xhtml";
	}

	return "/produto/consultaExcluiProduto.xhtml";
    }

    private boolean isValid(StringBuilder messageError) {
	if (StringUtils.isEmpty(entity.getNome())) {

	    messageError.append("Nome é obrigatório");
	}
	if (entity.getPreco() == null || BigDecimal.ZERO.equals(entity.getPreco())) {
	    if (messageError.length() > 0) {
		messageError.append(", ");
	    }
	    messageError.append("Preço é obrigatório");
	}
	if (messageError.length() == 0) {
	    return true;
	}
	return false;
    }

    public String novo() {
	entity = new ProdutoEntity();
	return "/produto/inseriAtualizaProduto.xhtml";
    }
}
