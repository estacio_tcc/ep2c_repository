package br.com.ep2c.view.convert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.convert.FacesConverter;

import br.com.ep2c.facade.FuncionarioEntityFacade;
import br.com.ep2c.model.FuncionarioEntity;

@FacesConverter("funcionarioConverter")
public class FuncionarioConverter extends PessoaConverter<FuncionarioEntity> {

    private Map<String, FuncionarioEntity> map;

    private FuncionarioEntityFacade facade;

    @Override
    protected Object getObject() {
	return new FuncionarioEntity();
    }

    public Map<String, FuncionarioEntity> getMap() {
	if (map == null || map.size() == 0) {
	    map = new HashMap<String, FuncionarioEntity>();

	    List<FuncionarioEntity> list = getFacade().list();
	    for (FuncionarioEntity f : list) {
		map.put(f.getNome(), f);
	    }
	}
	return map;
    }

    private FuncionarioEntityFacade getFacade() {
	if (facade == null) {
	    facade = new FuncionarioEntityFacade();
	}
	return facade;
    }
}
