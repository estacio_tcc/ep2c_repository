package br.com.ep2c.view.managedbean.dialog;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import br.com.ep2c.facade.ClienteEntityFacade;
import br.com.ep2c.model.ClienteEntity;

/**
 * <pre>
 *Fonte:
 *https://www.primefaces.org/showcase/ui/df/data.xhtml
 * </pre>
 */
@ManagedBean(name = "clienteDialog")
@ViewScoped
public class ClienteDialog {

    private static final Logger log = Logger.getLogger(ClienteDialog.class);

    private ClienteEntityFacade facade;

    private List<ClienteEntity> clientes;

    private ClienteEntityFacade getFacade() {
	if (facade == null) {
	    facade = new ClienteEntityFacade();
	}
	return facade;
    }

    @PostConstruct
    public void init() {
	log.debug("No metodo init");
	clientes = getFacade().list();
	log.debug("Size clientes" + (clientes != null ? clientes.size() : "is null"));
    }

    public void selectClienteFromDialog(ClienteEntity cliente) {
	log.debug("No metodo selectClienteFromDialog");
	RequestContext.getCurrentInstance().closeDialog(cliente);
    }

    public List<ClienteEntity> getClientes() {
	return clientes;
    }

    public void setClientes(List<ClienteEntity> clientes) {
	this.clientes = clientes;
    }

}
