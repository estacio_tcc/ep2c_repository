package br.com.ep2c.view.managedbean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import br.com.ep2c.facade.TransportadoraEntityFacade;
import br.com.ep2c.model.TransportadoraEntity;

@ManagedBean
@ViewScoped
public class ConsultaExcluiTransportadoraMB {

    private static final Logger log = Logger
	    .getLogger(ConsultaExcluiTransportadoraMB.class);

    private List<TransportadoraEntity> list;
    private TransportadoraEntityFacade facade;
    private TransportadoraEntity tmp;

    private TransportadoraEntityFacade getFacade() {
	if (facade == null) {
	    facade = new TransportadoraEntityFacade();
	}
	return facade;
    }

    @PostConstruct
    public void initialize() {
	log.debug("No metodo initialize");
	list = getFacade().list();
    }

    public List<TransportadoraEntity> getList() {
	return list;
    }

    public void setList(List<TransportadoraEntity> list) {
	this.list = list;
    }

    public String remove() {
	log.debug("No metodo edit");
	try {
	    log.debug("TransportadoraEntity[" + tmp + "]");
	    getFacade().delete(tmp);
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	}
	return "/transportadora/consultaExcluiTransportadora.xhtml";
    }

    public String edit() {
	log.debug("No metodo edit");
	try {
	    log.debug("TransportadoraEntity[" + tmp + "]");
	    FacesContext.getCurrentInstance().getExternalContext().getFlash()
		    .put("entity", tmp);

	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	    return "/transportadora/consultaExcluiTransportadora.xhtml";
	}
	return "/transportadora/inseriAtualizaTransportadora.xhtml";
    }

    public TransportadoraEntity getTmp() {
	return tmp;
    }

    public void setTmp(TransportadoraEntity tmp) {
	this.tmp = tmp;
    }
}
