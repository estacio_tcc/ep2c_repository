package br.com.ep2c.view.managedbean;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.ep2c.facade.ClienteEntityFacade;
import br.com.ep2c.model.ClienteEntity;
import br.com.ep2c.model.EnderecoEntity;

@ManagedBean
@ViewScoped
public class InseriAtualizaClienteMB implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2652161360363895079L;

    private ClienteEntity cliente;
    private ClienteEntityFacade facade;

    @PostConstruct
    public void loadCliente() {

	ClienteEntity c = (ClienteEntity) FacesContext.getCurrentInstance()
		.getExternalContext().getFlash().get("cliente");

	if (c == null) {
	    c = new ClienteEntity();
	    c.setEndereco(new EnderecoEntity());
	    c.setDataCriacao(new Date());
	}
	this.cliente = c;
    }

    public ClienteEntityFacade getFacade() {
	if (facade == null) {
	    facade = new ClienteEntityFacade();
	}
	return facade;
    }

    public ClienteEntity getCliente() {
	return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
	this.cliente = cliente;
    }

    public String salvar() {

	try {

	    getFacade().saveOrUpdate(cliente);
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    "Erro ao salvar cliente", "Erro cliente"));
	    e.printStackTrace();
	    return "/cliente/inseriAtualizaCliente.xhtml";
	}

	return "/cliente/consultaExcluiCliente.xhtml";
    }

    public String novo() {
	cliente = new ClienteEntity();
	return "/cliente/inseriAtualizaCliente.xhtml";
    }
}
