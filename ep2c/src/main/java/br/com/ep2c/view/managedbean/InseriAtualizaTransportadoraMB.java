package br.com.ep2c.view.managedbean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.ep2c.facade.TransportadoraEntityFacade;
import br.com.ep2c.model.EnderecoEntity;
import br.com.ep2c.model.TransportadoraEntity;
import br.com.ep2c.model.VeiculoEntity;

@ManagedBean
@ViewScoped
public class InseriAtualizaTransportadoraMB implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2652161360363895079L;

    private static final Logger log = Logger
	    .getLogger(InseriAtualizaTransportadoraMB.class);

    private TransportadoraEntity entity;
    private TransportadoraEntityFacade facade;

    private VeiculoEntity veiculoEntity;

    @PostConstruct
    public void loadEntity() {
	log.debug("No metodo loadEntity");

	TransportadoraEntity t = (TransportadoraEntity) FacesContext.getCurrentInstance()
		.getExternalContext().getFlash().get("entity");

	if (t == null) {
	    t = new TransportadoraEntity();
	    t.setEndereco(new EnderecoEntity());
	    if (t.getVeiculos() == null) {
		t.setVeiculos(new ArrayList<VeiculoEntity>());
	    }
	}
	if (veiculoEntity == null) {
	    veiculoEntity = new VeiculoEntity();
	}
	this.entity = t;
    }

    public TransportadoraEntityFacade getFacade() {
	if (facade == null) {
	    facade = new TransportadoraEntityFacade();
	}
	return facade;
    }

    public TransportadoraEntity getEntity() {
	return entity;
    }

    public void setEntity(TransportadoraEntity entity) {
	this.entity = entity;
    }

    public String salvar() {
	log.debug("No metodo salvar");

	try {
	    StringBuilder messageError = new StringBuilder();
	    if (isValid(messageError)) {
		getFacade().saveOrUpdate(entity);
	    } else {
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_ERROR,
				messageError.toString(), "Erro entity"));
		return "#";
	    }

	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	    return "#";
	}

	return "/transportadora/consultaExcluiTransportadora.xhtml";
    }

    private boolean isValid(StringBuilder messageError) {
	if (StringUtils.isEmpty(this.entity.getNomeTransportadora())) {
	    messageError.append("Nome da Transportadora é obrigatório");
	}
	if (StringUtils.isEmpty(this.entity.getCnpj())) {
	    if (messageError.length() > 0) {
		messageError.append(", ");
	    }
	    messageError.append("CNPJ é obrigatório");
	}
	if (messageError.length() > 0) {
	    return false;
	}
	return true;
    }

    public String novo() {
	entity = new TransportadoraEntity();
	return "/transportadora/inseriAtualizaTransportadora.xhtml";
    }

    public VeiculoEntity getVeiculoEntity() {
	return veiculoEntity;
    }

    public void setVeiculoEntity(VeiculoEntity veiculoEntity) {
	this.veiculoEntity = veiculoEntity;
    }

    public void removeVeiculo() {
	log.debug("No metodo removeVeiculo");
	this.entity.getVeiculos().remove(this.veiculoEntity);
    }

    public void editVeiculo() {
	log.debug("No metodo editVeiculo");
	this.entity.getVeiculos().remove(this.veiculoEntity);
    }

    public void add() {
	log.debug("No metodo add");
	StringBuilder messageError = new StringBuilder();
	if (isValidVeiculo(messageError)) {

	    try {
		VeiculoEntity e = (VeiculoEntity) this.veiculoEntity.clone();
		e.setTransportadora(this.entity);
		this.entity.getVeiculos().add(e);
	    } catch (CloneNotSupportedException e) {
		e.printStackTrace();
	    }
	    this.veiculoEntity = new VeiculoEntity();
	} else {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, messageError.toString(), "Erro entity"));
	}
    }

    private boolean isValidVeiculo(StringBuilder messageError) {
	if (StringUtils.isEmpty(this.veiculoEntity.getMontadora())) {
	    messageError.append("Motadora é obrigatório");
	}
	if (StringUtils.isEmpty(this.veiculoEntity.getModelo())) {
	    if (messageError.length() > 0) {
		messageError.append(", ");
	    }
	    messageError.append("Modelo é obrigatório");
	}
	if (StringUtils.isEmpty(this.veiculoEntity.getMontadora())) {
	    if (messageError.length() > 0) {
		messageError.append(", ");
	    }
	    messageError.append("Veículo é obrigatório");
	}
	if (messageError.length() > 0) {
	    return false;
	}
	return true;
    }
}
