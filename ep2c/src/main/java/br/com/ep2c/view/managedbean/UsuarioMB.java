package br.com.ep2c.view.managedbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.ep2c.facade.FuncionarioEntityFacade;
import br.com.ep2c.facade.LoginEntityFacade;
import br.com.ep2c.facade.UsuarioEntityFacade;
import br.com.ep2c.model.FuncionarioEntity;
import br.com.ep2c.model.LoginEntity;
import br.com.ep2c.model.Perfil;
import br.com.ep2c.model.UsuarioEntity;
import br.com.ep2c.util.Constants;
import br.com.ep2c.util.PasswdUtil;

@ManagedBean
@ViewScoped
public class UsuarioMB implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5224398763815832658L;

    private static final Logger log = Logger.getLogger(UsuarioMB.class);

    private List<UsuarioEntity> list;
    private UsuarioEntity entity;
    private String confirmacao;

    private List<FuncionarioEntity> funcionarios;
    private UsuarioEntityFacade facade;
    private FuncionarioEntityFacade funcionarioFacade;

    private LoginEntityFacade loginEntityFacade;

    private Perfil perfil;

    private UsuarioEntity tmp;

    public UsuarioEntityFacade getFacade() {
	if (facade == null) {
	    facade = new UsuarioEntityFacade();
	}
	return facade;
    }

    public FuncionarioEntityFacade getFuncionarioFacade() {
	if (funcionarioFacade == null) {
	    funcionarioFacade = new FuncionarioEntityFacade();
	}
	return funcionarioFacade;
    }

    public LoginEntityFacade getLoginEntityFacade() {
	if (loginEntityFacade == null) {
	    loginEntityFacade = new LoginEntityFacade();
	}
	return loginEntityFacade;
    }

    public String remove() {
	log.debug("No metodo remove");
	try {
	    log.debug("item[" + tmp + "]");
	    getFacade().delete(tmp);
	    initialize();
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao remover item", "Erro Pedido"));
	    e.printStackTrace();
	}
	return "#";
    }

    @PostConstruct
    public void initialize() {
	log.debug("No metodo initialize");

	HttpServletRequest rq = (HttpServletRequest) FacesContext.getCurrentInstance()
		.getExternalContext().getRequest();

	LoginMB mb = (LoginMB) rq.getSession().getAttribute(Constants.LOGIN_MB);

	list = loadUsuarios(mb);
	if (entity == null) {
	    initializeUsuarioEntity();
	}

	loadFuncionarios();
    }

    private List<UsuarioEntity> loadUsuarios(LoginMB mb) {
	UsuarioEntity e = getFacade().searchOneByField("login", mb.getLogin());
	boolean isAdmin = false;
	if (e.getPerfil().equals(Perfil.Admin.getCodigo())) {
	    isAdmin = true;
	}
	if (isAdmin) {
	    return getFacade().list();
	}
	List<UsuarioEntity> list = new ArrayList<UsuarioEntity>();
	UsuarioEntity tmp = getFacade().getUsuarioByPerfil(mb.getLogin());
	if (tmp != null) {
	    list.add(tmp);
	}
	return list;
    }

    private void loadFuncionarios() {
	log.debug("No metodo loadFuncionarios");
	List<FuncionarioEntity> funcs = getFuncionarioFacade().listByNotUsed();
	if (entity.getId() != null) {
	    funcs.add(entity.getFuncionario());
	}
	setFuncionarios(sortFuncionario(funcs));
    }

    public void novo() {
	log.debug("No metodo novo");
	initializeUsuarioEntity();
	loadFuncionarios();
    }

    private void initializeUsuarioEntity() {
	log.debug("No metodo initializeUsuarioEntity");
	entity = new UsuarioEntity();
	entity.setLogin(new LoginEntity());
	entity.setFuncionario(getFuncionarioEmpty());
    }

    public String editar() {
	log.debug("No metodo editar[" + tmp + "]");
	initializeUsuarioEntity();
	this.entity = this.tmp;
	loadFuncionarios();
	return "#";
    }

    public void salvar() {

	try {
	    if (validate()) {

		entity.getLogin()
			.setPassword(PasswdUtil.encript(entity.getLogin().getPassword()));

		getFacade().saveOrUpdate(entity);
		entity = null;
		initialize();
	    }
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	}
    }

    public void enableSenha() {

	try {
	    validate();

	    // getFacade().saveOrUpdate(entity);
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	}
    }

    private boolean validate() {
	StringBuilder builder = new StringBuilder();
	if (StringUtils.isEmpty(entity.getLogin().getLogin())) {

	    builder.append("Login é obrigatório");
	    builder.append(System.getProperty("line.separator"));
	} else {
	    if (entity.getLogin().getId() == null) {

		if (!getLoginEntityFacade().isValid(entity.getLogin().getLogin())) {
		    builder.append("Login já está em uso");
		    builder.append(System.getProperty("line.separator"));
		}
	    }
	}

	if (!StringUtils.equals(entity.getLogin().getPassword(), this.confirmacao)) {
	    builder.append("Erro na Senha e Confirmação de senha");
	    builder.append(System.getProperty("line.separator"));
	}
	if (StringUtils.isEmpty(entity.getLogin().getPassword())) {
	    builder.append("Senha é obrigatório");
	    builder.append(System.getProperty("line.separator"));
	}

	if (builder.length() > 0) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, builder.toString(), "Erro entity"));
	    return false;
	}
	return true;
    }

    public List<FuncionarioEntity> sortFuncionario(List<FuncionarioEntity> list) {

	Collections.sort(list, new Comparator<FuncionarioEntity>() {

	    public int compare(FuncionarioEntity o1, FuncionarioEntity o2) {
		return (o1.getNome()).compareTo((o2.getNome()));
	    }
	});
	return list;
    }

    private FuncionarioEntity getFuncionarioEmpty() {
	FuncionarioEntity f = new FuncionarioEntity();
	f.setNome("");
	return f;
    }

    public UsuarioEntity getEntity() {
	return entity;
    }

    public void setEntity(UsuarioEntity entity) {
	this.entity = entity;
    }

    public List<UsuarioEntity> getList() {
	return list;
    }

    public void setList(List<UsuarioEntity> list) {
	this.list = list;
    }

    public List<FuncionarioEntity> getFuncionarios() {
	return funcionarios;
    }

    public void setFuncionarios(List<FuncionarioEntity> funcionarios) {
	this.funcionarios = funcionarios;
    }

    public String getConfirmacao() {
	return confirmacao;
    }

    public void setConfirmacao(String confirmacao) {
	this.confirmacao = confirmacao;
    }

    public Perfil getPerfil() {
	return perfil;
    }

    public void setPerfil(Perfil perfil) {
	this.perfil = perfil;
    }

    public Perfil[] getPerfis() {
	HttpServletRequest rq = (HttpServletRequest) FacesContext.getCurrentInstance()
		.getExternalContext().getRequest();
	LoginMB mb = (LoginMB) rq.getSession().getAttribute(Constants.LOGIN_MB);
	UsuarioEntity e = getFacade().searchOneByField("login", mb.getLogin());
	if (e.getPerfil().equals(Perfil.Admin.getCodigo())) {
	    return Perfil.values();
	}
	Perfil[] p = { Perfil.UserFunc, Perfil.UserSystem };
	return p;
    }

    public UsuarioEntity getTmp() {
	return tmp;
    }

    public void setTmp(UsuarioEntity tmp) {
	this.tmp = tmp;
    }
}
