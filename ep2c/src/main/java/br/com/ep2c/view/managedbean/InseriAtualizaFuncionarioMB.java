package br.com.ep2c.view.managedbean;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.ep2c.facade.FuncionarioEntityFacade;
import br.com.ep2c.model.EnderecoEntity;
import br.com.ep2c.model.FuncionarioEntity;

@SuppressWarnings("restriction")
@ManagedBean
@ViewScoped
public class InseriAtualizaFuncionarioMB implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2652161360363895079L;

    private FuncionarioEntity entity;
    private FuncionarioEntityFacade facade;

    @PostConstruct
    public void loadEntity() {

	FuncionarioEntity p = (FuncionarioEntity) FacesContext.getCurrentInstance()
		.getExternalContext().getFlash().get("entity");

	if (p == null) {
	    p = new FuncionarioEntity();
	    p.setEndereco(new EnderecoEntity());
	    p.setDataCriacao(new Date());
	}
	this.entity = p;
    }

    public FuncionarioEntityFacade getFacade() {
	if (facade == null) {
	    facade = new FuncionarioEntityFacade();
	}
	return facade;
    }

    public FuncionarioEntity getEntity() {
	return entity;
    }

    public void setEntity(FuncionarioEntity entity) {
	this.entity = entity;
    }

    public String salvar() {

	try {

	    getFacade().saveOrUpdate(entity);
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	    return "/funcionario/inseriAtualizaFuncionario.xhtml";
	}

	return "/funcionario/consultaExcluiFuncionario.xhtml";
    }

    public String novo() {
	entity = new FuncionarioEntity();
	return "/funcionario/inseriAtualizaFuncionario.xhtml";
    }
}
