package br.com.ep2c.view.filter;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ep2c.model.LoginEntity;
import br.com.ep2c.view.managedbean.LoginMB;

public class SessionExpiredFilter implements Filter {
    private final static Logger logger = Logger
	    .getLogger(SessionExpiredFilter.class.getName());

    public void init(FilterConfig filterConfig) throws ServletException {
	if (logger.isLoggable(Level.FINE)) {
	    logger.fine("SessionExpiredFilter initiated successfully");
	}
    }

    public void doFilter(ServletRequest request, ServletResponse response,
	    FilterChain filterChain) throws IOException, ServletException {
	HttpServletRequest rq = (HttpServletRequest) request;
	HttpServletResponse rp = (HttpServletResponse) response;
	boolean auth = checkAuthentication(rq);
	if (!auth && !rq.getRequestURI().contains("ep2c/index.xhtml")) {
	    String[] split = rq.getRequestURI().split("ep2c");
	    if (split.length > 0) {
		rp.sendRedirect("/ep2c/index.xhtml");
	    }
	} else {
	    try {
		filterChain.doFilter(request, response);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }

    private boolean checkAuthentication(HttpServletRequest rq) {
	LoginMB mb = (LoginMB) rq.getSession().getAttribute("loginMB");
	if (mb != null && mb.getLogin() != null) {
	    LoginEntity entity = mb.getLogin();
	    if (entity.getId() != null && entity.getId() > 0) {
		return true;
	    }
	}
	return false;
    }

    public void destroy() {
	if (logger.isLoggable(Level.FINE)) {
	    logger.fine("Destroying SessionExpiredFilter");
	}
    }

}
