package br.com.ep2c.view.managedbean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import br.com.ep2c.facade.ClienteEntityFacade;
import br.com.ep2c.facade.OrdemEntregaEntityFacade;
import br.com.ep2c.model.ClienteEntity;
import br.com.ep2c.model.FuncionarioEntity;
import br.com.ep2c.model.OrdemEntregaEntity;
import br.com.ep2c.model.VeiculoEntity;
import br.com.ep2c.util.Constants;

@ManagedBean
@ViewScoped
public class InseriAtualizaOrdemEntregaMB implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2652161360363895079L;

    private static final Logger log = Logger
	    .getLogger(InseriAtualizaOrdemEntregaMB.class);

    private OrdemEntregaEntity entity;
    private OrdemEntregaEntityFacade facade;

    private List<ClienteEntity> clientes;
    private ClienteEntityFacade clienteFacade;

    public ClienteEntityFacade getClienteFacade() {
	if (clienteFacade == null) {
	    clienteFacade = new ClienteEntityFacade();
	}
	return clienteFacade;
    }

    @PostConstruct
    public void init() {
	log.debug("No metodo loadEntity");

	OrdemEntregaEntity o = (OrdemEntregaEntity) FacesContext.getCurrentInstance()
		.getExternalContext().getFlash().get("entity");

	clientes = getClienteFacade().list();

	if (o == null) {
	    o = new OrdemEntregaEntity();
	    o.setCliente(new ClienteEntity());
	    o.setVeiculo(new VeiculoEntity());
	    o.setMotorista(new FuncionarioEntity());
	}
	this.entity = o;
    }

    public OrdemEntregaEntityFacade getFacade() {
	if (facade == null) {
	    facade = new OrdemEntregaEntityFacade();
	}
	return facade;
    }

    public OrdemEntregaEntity getEntity() {
	return entity;
    }

    public void setEntity(OrdemEntregaEntity entity) {
	this.entity = entity;
    }

    public String salvar() {
	log.debug("No metodo salvar");

	try {
	    HttpServletRequest rq = (HttpServletRequest) FacesContext.getCurrentInstance()
		    .getExternalContext().getRequest();
	    LoginMB mb = (LoginMB) rq.getSession().getAttribute(Constants.LOGIN_MB);
	    // UsuarioEntity usuario =
	    // getUsuarioFacade().getUsuarioByPerfil(mb.getLogin());
	    // entity.setFuncionario(usuario.getFuncionario());
	    getFacade().saveOrUpdate(entity);
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	    return "#";
	}

	return "#";
    }

    public String novo() {
	entity = new OrdemEntregaEntity();
	return "/ordementrega/inseriAtualizaOrdemEntrega.xhtml";
    }

    public void searchCliente() {
	log.debug("No metodo searchCliente");
	Map<String, Object> options = new HashMap<String, Object>();
	options.put("resizable", false);
	options.put("draggable", false);
	options.put("modal", true);
	RequestContext.getCurrentInstance().openDialog("cliente/clienteDialog.xhtml",
		options, null);
    }

    public void onClienteChosen(SelectEvent event) {
	log.debug("No metodo searchCliente");
	ClienteEntity cliente = (ClienteEntity) event.getObject();
	FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
		"Cliente Selecionado", cliente.getId() + ", " + cliente.getNome());
	FacesContext.getCurrentInstance().addMessage(null, message);

    }

    public void searchMotorista() {
	log.debug("No metodo searchMotorista");
    }

    public void searchVeiculo() {
	log.debug("No metodo searchVeiculo");
    }

    public List<ClienteEntity> getClientes() {
	return clientes;
    }

    public void setClientes(List<ClienteEntity> clientes) {
	this.clientes = clientes;
    }

}
