package br.com.ep2c.view.managedbean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.ep2c.facade.ClienteEntityFacade;
import br.com.ep2c.facade.PedidoEntityFacade;
import br.com.ep2c.facade.ProdutoEntityFacade;
import br.com.ep2c.facade.UsuarioEntityFacade;
import br.com.ep2c.model.ClienteEntity;
import br.com.ep2c.model.ItemPedidoEntity;
import br.com.ep2c.model.PedidoEntity;
import br.com.ep2c.model.ProdutoEntity;
import br.com.ep2c.model.UsuarioEntity;
import br.com.ep2c.util.Constants;

@ManagedBean
@ViewScoped
public class InseriAtualizaPedidoMB implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2652161360363895079L;

    private static final Logger log = Logger.getLogger(InseriAtualizaPedidoMB.class);

    private PedidoEntity entity;
    private List<ClienteEntity> clientes;
    private String nomeProduto;
    private BigDecimal quantidade;

    private ClienteEntityFacade clienteFacade;
    private PedidoEntityFacade facade;
    private ProdutoEntityFacade produtoFacade;

    private ItemPedidoEntity tmp;

    private UsuarioEntityFacade usuarioFacade;

    @PostConstruct
    public void loadEntity() {

	PedidoEntity p = (PedidoEntity) FacesContext.getCurrentInstance()
		.getExternalContext().getFlash().get("entity");

	this.clientes = getClienteFacade().list();

	if (p == null) {
	    p = new PedidoEntity();
	}
	this.entity = p;
	if (this.entity.getItens() == null) {
	    this.entity.setItens(new ArrayList<ItemPedidoEntity>());
	}
    }

    public ClienteEntityFacade getClienteFacade() {
	if (clienteFacade == null) {
	    clienteFacade = new ClienteEntityFacade();
	}
	return clienteFacade;
    }

    public PedidoEntityFacade getFacade() {
	if (facade == null) {
	    facade = new PedidoEntityFacade();
	}
	return facade;
    }

    public ProdutoEntityFacade getProdutoFacade() {
	if (produtoFacade == null) {
	    produtoFacade = new ProdutoEntityFacade();
	}
	return produtoFacade;
    }

    public PedidoEntity getEntity() {
	return entity;
    }

    public void setEntity(PedidoEntity entity) {
	this.entity = entity;
    }

    public List<ClienteEntity> getClientes() {
	return clientes;
    }

    public void setClientes(List<ClienteEntity> clientes) {
	this.clientes = clientes;
    }

    public String salvar() {

	try {
	    HttpServletRequest rq = (HttpServletRequest) FacesContext.getCurrentInstance()
		    .getExternalContext().getRequest();
	    LoginMB mb = (LoginMB) rq.getSession().getAttribute(Constants.LOGIN_MB);
	    UsuarioEntity usuario = getUsuarioFacade().getUsuarioByPerfil(mb.getLogin());
	    entity.setFuncionario(usuario.getFuncionario());
	    getFacade().saveOrUpdate(entity);
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao salvar entity", "Erro entity"));
	    e.printStackTrace();
	    return "#";
	}

	return "#";
    }

    public UsuarioEntityFacade getUsuarioFacade() {
	if (usuarioFacade == null) {
	    usuarioFacade = new UsuarioEntityFacade();
	}
	return usuarioFacade;
    }

    public String add() {

	ProdutoEntity produto = null;
	StringBuilder errors = new StringBuilder();
	if (StringUtils.isEmpty(nomeProduto)) {
	    errors.append("Produto é obrigatório");
	} else {
	    produto = getProdutoFacade().findByNome(nomeProduto);
	    if (produto == null) {
		errors.append("Não foi localizado o produto");
	    }
	}
	if (quantidade == null || quantidade.equals(BigDecimal.ZERO)) {
	    errors.append("Quantidade é Obrigatório");
	}
	if (errors.length() > 0) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, errors.toString(), errors.toString()));
	    return "#";
	}
	add(createItemPedidoEntity(produto), this.entity.getItens());
	this.quantidade = null;
	this.nomeProduto = "";

	return "#";
    }

    private void add(ItemPedidoEntity item, List<ItemPedidoEntity> itens) {
	if (itens.size() == 0) {
	    itens.add(item);
	}
	boolean achou = false;
	for (ItemPedidoEntity i : itens) {
	    if (i.getProduto().equals(item.getProduto())) {
		i.setQuantidade(i.getQuantidade() + item.getQuantidade());
		achou = true;
		break;
	    }
	}
	if (!achou) {
	    itens.add(item);
	}
	atualizaValorTotal();
    }

    private ItemPedidoEntity createItemPedidoEntity(ProdutoEntity produto) {
	ItemPedidoEntity item = new ItemPedidoEntity();
	item.setPrecoUnitario(produto.getPreco());
	item.setQuantidade(this.quantidade.doubleValue());
	item.setProduto(produto);
	item.setPedido(this.entity);
	return item;
    }

    public String remove() {
	log.debug("No metodo edit");
	try {
	    log.debug("item[" + tmp + "]");
	    this.getEntity().getItens().remove(tmp);
	    atualizaValorTotal();
	} catch (Throwable e) {
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Erro ao remover item", "Erro Pedido"));
	    e.printStackTrace();
	}
	return "#";
	// return "/pedido/inseriAtualizaPedido.xhtml";
    }

    public List<String> completeText(String nome) {
	List<String> results = new ArrayList<String>();
	if (nome.length() > 0) {
	    results = getProdutoFacade().findByLikeNome(nome);
	}

	return results;
    }

    public String novo() {
	entity = new PedidoEntity();
	return "/pedido/inseriAtualizaPedido.xhtml";
    }

    public String getNomeProduto() {
	return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
	this.nomeProduto = nomeProduto;
    }

    public BigDecimal getQuantidade() {
	return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
	this.quantidade = quantidade;
    }

    private void atualizaValorTotal() {
	if (this.entity.getItens() == null || this.entity.getItens().size() == 0) {
	    this.entity.setValorTotal(null);
	} else {

	    BigDecimal bd = new BigDecimal(0);
	    for (ItemPedidoEntity i : this.entity.getItens()) {
		bd = bd.add(i.getPrecoUnitario()
			.multiply(BigDecimal.valueOf(i.getQuantidade())));
	    }
	    this.entity.setValorTotal(bd);
	}
    }

    public ItemPedidoEntity getTmp() {
	return tmp;
    }

    public void setTmp(ItemPedidoEntity tmp) {
	this.tmp = tmp;
    }

}
