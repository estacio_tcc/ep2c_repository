package br.com.ep2c.view.convert;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import br.com.ep2c.model.PessoaEntity;

public abstract class PessoaConverter<E extends PessoaEntity> implements Converter {

    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
	if (value != null) {
	    return getMap().get(value);
	} else {
	    return getObject();
	}
    }

    protected Object getObject() {
	return null;
    }

    public abstract Map<String, E> getMap();

    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
	if (object != null && object instanceof PessoaEntity) {

	    @SuppressWarnings("unchecked")
	    E entity = ((E) object);

	    return entity.getNome();
	} else {
	    return "";
	}
    }
}
