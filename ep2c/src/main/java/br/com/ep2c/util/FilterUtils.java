package br.com.ep2c.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class FilterUtils {

    public static void redirectToUrl(HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse, String url, int requestStatus)
            throws IOException {
        if (isAJAXRequest(httpServletRequest)) {
            StringBuilder sb = new StringBuilder();
            sb.append(xmlPartialRedirectToPage(url));
            httpServletResponse.setHeader("Cache-Control", "no-cache");
            httpServletResponse.setCharacterEncoding("UTF-8");
            httpServletResponse.setContentType("text/xml");
            PrintWriter pw = httpServletResponse.getWriter();
            pw.println(sb.toString());
            pw.flush();

        } else {
            httpServletResponse.setStatus(requestStatus);
            httpServletResponse.sendRedirect(url);
        }
    }

    private static boolean isAJAXRequest(HttpServletRequest request) {
        boolean check = false;
        String facesRequest = request.getHeader("Faces-Request");
        if (facesRequest != null && facesRequest.equals("partial/ajax")) {
            check = true;
        }
        return check;
    }

    private static String xmlPartialRedirectToPage(String redirectUrl) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><partial-response><redirect url=\"")
                .append(redirectUrl).append("\"></redirect></partial-response>");
        return sb.toString();
    }
}
