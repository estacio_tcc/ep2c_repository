package br.com.ep2c.util;

import java.security.MessageDigest;

import org.apache.log4j.Logger;

public class PasswdUtil {

	private static final String MESSAGE_ERROR = "Ocorreu um erro na execução do metodo encript";
	private static Logger logger = Logger.getLogger(PasswdUtil.class);

	public static String encript(String value) {
		logger.debug("No metodo encript");
		String senha = null;
		try {
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			byte messageDigest[] = algorithm.digest(value.getBytes("UTF-8"));
			StringBuilder hexString = new StringBuilder();
			for (byte b : messageDigest) {
				hexString.append(String.format("%02X", 0xFF & b));
			}
			senha = hexString.toString();
		} catch (Exception e) {
			logger.error(MESSAGE_ERROR);
			e.printStackTrace();
			throw new RuntimeException(MESSAGE_ERROR, e);
		}
		return senha;
	}

	public static void main(String[] args) {
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				System.out.println("Value[" + encript(args[i].trim()) + "]");
			}
		}
	}
}
