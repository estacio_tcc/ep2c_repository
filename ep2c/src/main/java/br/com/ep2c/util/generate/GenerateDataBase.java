package br.com.ep2c.util.generate;

import java.util.Date;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

import br.com.ep2c.facade.LoginEntityFacade;
import br.com.ep2c.facade.UsuarioEntityFacade;
import br.com.ep2c.model.FuncionarioEntity;
import br.com.ep2c.model.LoginEntity;
import br.com.ep2c.model.Perfil;
import br.com.ep2c.model.UsuarioEntity;
import br.com.ep2c.util.PasswdUtil;

public class GenerateDataBase {

    private static Logger logger = Logger.getLogger(GenerateDataBase.class);

    public static void main(String[] args) {
	logger.debug("Inicio da geração de tabelas");
	EntityManagerFactory factory = Persistence.createEntityManagerFactory("ep2c");
	factory.close();
	logger.debug("Fim da geração de tabelas");

	logger.debug("Criando perfil Administrador");
	LoginEntity loginEntity = new LoginEntity();
	loginEntity.setLogin("admin");
	loginEntity.setPassword(PasswdUtil.encript("1234"));

	FuncionarioEntity funcionarioEntity = new FuncionarioEntity();
	funcionarioEntity.setNome("Admin");
	funcionarioEntity.setDataCriacao(new Date());
	funcionarioEntity.setCpfCnpj("N/A");

	UsuarioEntity entity = new UsuarioEntity();
	entity.setLogin(loginEntity);
	entity.setFuncionario(funcionarioEntity);
	entity.setPerfil(Perfil.Admin.getCodigo());
	logger.debug("Fim da criação do perfil");
	logger.debug("Inserindo informações na base de dados");

	UsuarioEntityFacade facade = new UsuarioEntityFacade();
	LoginEntityFacade loginFacade = new LoginEntityFacade();
	try {
	    if (loginFacade.isValid(entity.getLogin().getLogin())) {
		facade.saveOrUpdate(entity);
	    }
	} catch (Exception e) {
	    logger.error("Erro na criação do perfil");
	    e.printStackTrace();
	}
	logger.error("Fim da execução");
    }
}
