package br.com.ep2c.util;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil {

    private static Logger log = Logger.getLogger(HibernateUtil.class);

    private static final SessionFactory sessionFactory;
    @SuppressWarnings({ "rawtypes" })
    private static final ThreadLocal session = new ThreadLocal();
    static {
	try {
	    log.debug("Criando ThreadLocal");

	    Configuration config = new Configuration().configure("hibernate-cfg.xml");
	    ServiceRegistry sr = new ServiceRegistryBuilder()
		    .applySettings(config.getProperties()).buildServiceRegistry();
	    sessionFactory = config.buildSessionFactory(sr);
	} catch (Throwable ex) {
	    ex.printStackTrace();
	    System.err.println("Initial SessionFactory creation failed." + ex);
	    throw new ExceptionInInitializerError(ex);
	}
    }

    @SuppressWarnings("unchecked")
    public static Session getCurrentSession() {
	Session s = (Session) session.get();
	if (s == null || !s.isOpen()) {
	    s = sessionFactory.openSession();
	}
	session.set(s);
	return s;
    }

    public static Session getSession() {
	return getCurrentSession();
    }

    @SuppressWarnings("unchecked")
    public static void closeSession() {
	Session s = (Session) session.get();
	if (s != null && s.isOpen()) {
	    s.close();
	}
	session.set(s);
    }
}
